#-------------------------------------------------
#
# Project created by QtCreator 2017-06-20T12:46:18
#
#-------------------------------------------------

TARGET = wfile
TEMPLATE = lib
CONFIG += staticlib c++1z
CONFIG -= qt
QMAKE_CXXFLAGS += -std=c++17

QMAKE_CXXFLAGS += -fPIC -fvisibility=hidden -fvisibility-inlines-hidden

SOURCES += \
    wfile.cpp

HEADERS += \
    rdi_wfile.hpp

INCLUDEPATH += /opt/rdi/include/
