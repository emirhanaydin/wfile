#include <rdi_stl_utils.hpp>
#include <rdi_wfile.hpp>

#include <sstream>
#include <codecvt>
#include <utility>
#include <algorithm>
#include <memory>
#include <experimental/filesystem>

using namespace std;
namespace fs = std::experimental::filesystem;
namespace RDI
{

wstring read_wfile(const string& filename)
{
	throw_unless(file_exists(filename),
				 std::runtime_error(filename + " file doesn't exist!"));

	wifstream wif(filename.c_str());
	wif.imbue(locale(wif.getloc(), new codecvt_utf8<wchar_t>));
	wstringstream wss;
	wss << wif.rdbuf();
	return wss.str();
}

bool write_wfile(const string& filename, const wstring& fileContent)
{
	wofstream wof(filename.c_str());

	if(!wof.is_open())
	{
		return false;
	}

	wof.imbue(locale(wof.getloc(), new codecvt_utf8<wchar_t>));
	wof << fileContent;
	return true;
}

string read_file(const string &filename)
{
	throw_unless(file_exists(filename),
				 std::runtime_error(filename + " file doesn't exist!"));

	ifstream stream(filename.c_str());
	stringstream ss;
	ss << stream.rdbuf();
	return ss.str();
}

std::vector<char>
read_binary_file(const std::string& filename)
{
	ifstream stream (filename, std::ios::in | std::ios::binary);
	vector<char> file_bytes;

	stringstream ss;
	ss << stream.rdbuf();
	for(auto c : ss.str())
	{
		file_bytes.emplace_back(c);
	}

	return file_bytes;
}

void
write_binary_file(const std::string& filename, const std::vector<char>& content)
{
	ofstream stream (filename, std::ios::out | std::ios::binary);

	for(auto c : content)
	{
		stream << c;
	}
}

bool write_file(const string &filename, const string &fileContent)
{
	ofstream of(filename.c_str());

	if(!of.is_open())
	{
		return false;
	}

	of << fileContent;
	return true;
}


string get_current_directory()
{

	return fs::current_path().string() + "/";
}

std::vector<string> find_files_that(const fs_path& dir,
									std::function<bool(const fs_path&)> filter,
									const bool recurse,
									const bool follow_symlinks)
{

	std::vector<string> result;

	if (not fs::exists(dir) or not is_directory(dir))
	{
		return result;
	}

	auto dir_ops = follow_symlinks
		? fs::directory_options::follow_directory_symlink
		: fs::directory_options::none;

	if (recurse)
	{
		for (const auto& entry : fs::recursive_directory_iterator(dir, dir_ops))
		{
			if (fs::is_regular_file(entry) && filter(entry))
			{
				result.push_back(entry.path());
			}
		}
	}
	else
	{
		for (const auto& entry : fs::directory_iterator(dir, dir_ops))
		{
			if (fs::is_regular_file(entry) && filter(entry))
			{
				result.push_back(entry.path());
			}
		}
	}

	return result;
}

vector<wstring> read_wfile_lines(const string &filename)
{
	throw_unless(file_exists(filename),
				 std::runtime_error(filename + " file doesn't exist!"));

	std::setlocale(LC_ALL, "en_US.UTF-8");
	wifstream wif(filename.c_str());
	wif.imbue(locale(wif.getloc(), new codecvt_utf8<wchar_t>));
	wstringstream wss;
	wss << wif.rdbuf();

	wstring tmp;
	vector<wstring> lines;
	while (getline(wss, tmp))
		lines.push_back(tmp);

	return lines;
}

bool write_wfile_lines(const string &filename,
					   const vector<wstring> &lines_to_write)
{
	wofstream wof(filename.c_str());

	if(!wof.is_open())
	{
		return false;
	}

	wof.imbue(locale(wof.getloc(), new codecvt_utf8<wchar_t>));

	for (const wstring& line : lines_to_write)
	{
		wof << line << endl;
	}

	return true;
}

vector<string> read_file_lines(const string &filename)
{
	throw_unless(file_exists(filename),
				 std::runtime_error(filename + " file doesn't exist!"));

	ifstream stream(filename.c_str());
	stringstream ss;
	ss << stream.rdbuf();

	string tmp;
	vector<string> lines;

	while (getline(ss, tmp))
	{
		lines.push_back(tmp);
	}

	return lines;
}

bool write_file_lines(const string &filename,
					  const vector<string> &lines_to_write)
{
	ofstream of(filename.c_str());

	if(!of.is_open())
	{
		return false;
	}

	for (const string& line : lines_to_write)
	{
		of << line << endl;
	}

	return true;
}

vector<string> get_directory_content(STRING_VIEW path)
{
	vector<string> output;

	transform(fs::directory_iterator(path),
			  fs::directory_iterator(),
			  back_inserter(output),
			  [](const fs::directory_entry& entry) {
				  return entry.path().filename();
			  });

	sort(begin(output), end(output));
	return output;
}

bool append_to_wfile(const string& filename, const wstring& content)
{
	if(!file_exists(filename))
	{
		write_wfile(filename, L"");
	}

	wofstream wof;
	wof.open(filename.c_str(), ios_base::app);

	if(!wof.is_open())
	{
		return false;
	}

	wof.imbue(locale(wof.getloc(), new codecvt_utf8<wchar_t>));
	wof << content << '\n';
	return true;
}

bool append_to_file(const string &filename, const string &content)
{
	if(!file_exists(filename))
	{
		write_file(filename, "");
	}

	ofstream of;
	of.open(filename.c_str(), ios_base::app);

	if(!of.is_open())
	{
		return false;
	}

	of << content << '\n';
	return true;
}

string get_absolute_path(STRING_VIEW path)
{
	return fs::absolute(path).string();
}

bool create_directory(STRING_VIEW path, bool nested)
{
	return nested ? fs::create_directories(path):
					fs::create_directory(path);
}


bool delete_directory(STRING_VIEW path)
{
	return  fs::remove(path);
}

bool delete_file(STRING_VIEW path)
{
	if(!file_exists(path))
	{
		return false;
	}

	return fs::remove(path);
}

bool dump_matrix(const std::string &file_name,
				 std::vector<std::vector<float>> &input_matrix)
{
	std::vector<std::string> content;

	for(const auto& inner : input_matrix)
	{
		std::string line;

		for(const auto& item : inner)
		{
			line += std::to_string(item);
			line += " ";
		}

		content.push_back(line);
	}

	return write_file_lines(file_name, content);
}

bool file_exists(STRING_VIEW filename)
{
	return fs::exists(filename);
}

bool is_directory(STRING_VIEW  path)
{
	return fs::is_directory(path);
}

std::string extract_path_from_filename(STRING_VIEW filename)
{
	return fs::path(filename).parent_path().string() +'/';
}

std::string extract_filename(const std::string& path)
{
	throw_unless(!is_directory(path),
				 std::runtime_error(path + " is a directory."));

	return fs::path(path).filename().string();
}

std::string extract_filename_without_extension(const std::string& path)
{
	throw_unless(!is_directory(path),
				 std::runtime_error(path + " is a directory."));

	return fs::path(path).stem().string();
}

std::string extract_extention_from_path(STRING_VIEW filename)
{
	return fs::path(filename).extension();
}

} //namespace RDI
