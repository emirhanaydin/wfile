#ifndef RDI_WFILE_HPP
#define RDI_WFILE_HPP

#if __GNUC__ >= 7
#include <string_view>
#define STRING_VIEW std::string_view
#else
#include <experimental/string_view>
#define STRING_VIEW std::experimental::string_view
#endif

#include <experimental/filesystem>
#include <fstream>
#include <functional>
#include <iostream>
#include <map>
#include <string>
#include <vector>


namespace RDI
{

using fs_path = std::experimental::filesystem::path;

std::wstring read_wfile(const std::string& filename);

std::vector<std::wstring> read_wfile_lines(const std::string& filename);

std::string read_file(const std::string& filename);

std::vector<std::string> read_file_lines(const std::string& filename);

std::vector<char> read_binary_file(const std::string& filename);

bool write_wfile(const std::string& filename, const std::wstring& fileContent);

bool append_to_wfile(const std::string& filename, const std::wstring& content);

bool write_wfile_lines(const std::string& filename,
					   const std::vector<std::wstring>& linesToWrite);

bool write_file(const std::string& filename, const std::string& fileContent);

void
write_binary_file(const std::string& filename, const std::vector<char>& content);

bool append_to_file(const std::string& filename, const std::string& content);

bool write_file_lines(const std::string& filename,
					  const std::vector<std::string>& linesToWrite);

template <typename T, template <typename...> class Map, typename K, typename V>
bool write_map(const std::string& filename, const Map<K, V>& mapToWrite)
{
	std::basic_ofstream<T> of(filename.c_str());
	if (!of.is_open())
		return false;

	for (std::pair<K, V> line : mapToWrite)
		of << line.first << " " << line.second << std::endl;

	return true;
}

template <typename T, template <typename...> class Map, typename K, typename V>
bool write_map(const std::string& filename,
			   const Map<K, std::vector<V>>& mapToWrite)
{
	std::basic_ofstream<T> of(filename.c_str());
	if (!of.is_open())
		return false;

	for (std::pair<K, std::vector<V>> line : mapToWrite)
	{
		of << line.first << std::endl;
		for (V value : line.second)
			of << value << std::endl;
	}
	return true;
}

///  return absolute pathes to files that mathces the predicate
///	 auto files = find_files_that(path); will return all files in that path
///  auto files = find_files_that(asdf,[](const fs::path &p)
///								 {return p.extension() == ".cpp";});
///  will return all files in that path that ends with .cpp
std::vector<std::string>
find_files_that(const fs_path& dir,
				std::function<bool(const fs_path&)> filter
				= [](const fs_path&) { return true; },
				const bool recurse = false, const bool follow_symlinks = false);

bool delete_file(STRING_VIEW path);

std::string get_absolute_path(STRING_VIEW);

/// returns the location of the binary executable ex: "/home/rdi/bin"
std::string get_current_directory();

std::vector<std::string> get_directory_content(STRING_VIEW path);

bool create_directory(STRING_VIEW path, bool nested = false);

bool delete_directory(STRING_VIEW path);

bool dump_matrix(const std::string& file_name,
				 std::vector<std::vector<float>>& input_matrix);

bool file_exists(STRING_VIEW filename);

bool is_directory(STRING_VIEW path);

std::string extract_path_from_filename(STRING_VIEW filename);

std::string extract_filename(const std::string& path);

std::string extract_filename_without_extension(const std::string& path);

std::string extract_extention_from_path(STRING_VIEW filename);

} // namespace RDI

#endif // RDI_WFILE_HPP
