TEMPLATE = app
CONFIG += c++1z
QMAKE_CXXFLAGS += -std=c++1z
CONFIG -= app_bundle
CONFIG -= qt

CONFIG(release, debug|release) {
        unix:!macx: LIBS += -L$$/opt/rdi/lib
        DEFINES +=
}
CONFIG(debug, debug|release) {
        unix:!macx: LIBS += -L$$/opt/rdi/lib_debug
}

equals(CI, "enabled") {
    message(CI mode is enabled)
    DEFINES+= __CI__
}

SOURCES += \
    main.cpp

DEFINES += CODE_LOCATION="\\\"$$PWD/\\\""

INCLUDEPATH += /opt/rdi/include
INCLUDEPATH += ../lib

LIBS += -L$$OUT_PWD/../lib
LIBS += -lwfile              \
                    -lstdc++fs
